/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package simplegauss;
import java.awt.EventQueue;
import java.awt.GridLayout;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StreamTokenizer;
import java.util.Arrays;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
/**
 * @author maksim
 */
public class SimpleGauss extends JFrame {
    /**
     * @param args
     * @throws IOException 
     */
    //double m =0.1;
    public static void main(String[] args) throws IOException{
       StreamTokenizer in = new StreamTokenizer(new InputStreamReader(System.in));// razborshik
       in.nextToken();
       int n=(int)in.nval;
       a=new double[n][n+1];
       for(int i=0;i<n;i++)
           for(int j=0;j<=n;j++){
               in.nextToken();
               a[i][j]=(double)in.nval;
           }
       invoke(null);
       invoke(gauss(a));
    }
    /**
     * @param p
     */
    public SimpleGauss(double[] p){
        boolean v = p!=null;
        int n=v?p.length:a.length,m=v?1:a[0].length;
        setTitle(v?"roots":"source");
        JPanel panel = new JPanel();
        panel.setLayout(new GridLayout(n,m));
        for(int i=0;i<n;i++){
            for(int j=0;j<m;j++){
                panel.add(new JLabel(String.valueOf(v?p[i]:a[i][j])));
                System.out.printf("%8.5f",a[i][j]);
            }
            System.out.printf("%n");
        }
        add(panel);
        pack();
        setLocationRelativeTo(null);
    }
    /**
     * @param arg0
     */
    private static void invoke(final double[] a) {
        EventQueue.invokeLater(new Runnable(){
           @Override
           public void run(){
               SimpleGauss f=new SimpleGauss(a);
               f.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
               f.setVisible(true);
          }
        });
    }
    /**
     * @param a
     * @return
     */
    public static double[] gauss(double[][] a){
            double[][] x = new double[a.length][a[0].length];
            for(int i=0;i<a.length;i++)
                x[i]=Arrays.copyOf(a[i],a[i].length);
            double p;
            for(int i=0;i<x.length;i++)
                for(int j=0;j<x.length;j++)
                    if(i!=j){       
                        p=x[j][i]/x[i][i];
                        for(int k=i;k<=x.length;++k)
                            x[j][k]-=p*x[i][k];
                    }
            double[] b=new double[x.length];
            for(int i=0;i<x.length;i++)
                b[i]=x[i][x.length]/x[i][i];
            return b;
    }
    private static double[][] a = null;
    //private static final long serialVersionUID = 1L;
}